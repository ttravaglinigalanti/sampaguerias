from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse


def index(request):
    context = {}
    return render(request, 'Teste/index.html', context)

def about(request):
    context = {}
    return render(request, 'Teste/about.html', context)