from django.db import models
from django.conf import settings


class Post(models.Model):
    name = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    data = models.DateTimeField(auto_now_add=True)
    opnion = models.TextField(max_length=2047)
    poster_url = models.URLField(max_length=200, null=True)

    def __str__(self):
        return f'{self.name}'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    data = models.DateTimeField(auto_now_add=True)
    hamburguer = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

Comment.objects.order_by("data")

class Category(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    hamburgueria = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.name} by {self.author}'
