from django.apps import AppConfig


class HamburguerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hamburguer'
