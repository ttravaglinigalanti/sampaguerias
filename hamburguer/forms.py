from django import forms
from django.forms import ModelForm
from .models import Post, Comment

class HamburguerForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'name',
            'location',
            'opnion',
            'poster_url',
        ]
        labels = {
            'name': 'Hamburgueria',
            'location': 'Localização',
            'opnion' : 'O que você achou desse lugar?',
            'poster_url' : 'URL da Foto',
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Usuário',
            'text': 'Comentário',
        }
