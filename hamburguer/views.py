from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from .models import Post, Comment, Category
from .forms import HamburguerForm, CommentForm
from django.shortcuts import render, get_object_or_404
from django.views import generic

class HamburguerDetailView(generic.DetailView):
    model = Post
    template_name = 'hamburguer/detail.html'
    context_object_name = 'hamburguer'


class HamburguerListView(generic.ListView):
    model = Post
    template_name = 'hamburguer/index.html'
    context_object_name = 'hamburguer_list'
   
def search_hamburguer(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        hamburguer_list = Post.objects.filter(name__icontains=search_term)
        context = {"hamburguer_list": hamburguer_list}
    return render(request, 'hamburguer/search.html', context)

class HamburguerCreateView(generic.CreateView):
    model = Post
    template_name = 'hamburguer/create.html'
    form_class = HamburguerForm
    
    def get_success_url(self):
        return (reverse('hamburguer:detail', args=(self.object.id, )))


class HamburguerUpdateView(generic.UpdateView):
    model = Post
    template_name = 'hamburguer/update.html'
    context_object_name = 'hamburguer'
    fields = ['name', 'location', 'opnion', 'poster_url']

    def get_success_url(self):
        return reverse('hamburguer:detail', args=(self.object.id, ))


class HamburguerDeleteView(generic.DeleteView):
    model = Post
    template_name = 'hamburguer/delete.html'
    context_object_name = 'hamburguer'
    success_url = reverse_lazy('hamburguer:index')

def create_comment(request, hamburguer_id):
    hamburguer = get_object_or_404(Post, pk=hamburguer_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            hamburguer=hamburguer)
            comment.save()
            return HttpResponseRedirect(
                reverse('hamburguer:detail', args=(hamburguer_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'hamburguer': hamburguer}
    return render(request, 'hamburguer/create_comment.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'hamburguer/lists.html'


class CategoryCreateView(generic.CreateView):
    model = Category
    template_name = 'hamburguer/create_list.html'
    fields = ['name', 'author', 'hamburgueria']
    success_url = reverse_lazy('hamburguer:lists')

class CategoryDetailView(generic.DetailView):
    model = Category
    template_name = 'hamburguer/category_detail.html'
    context_object_name = 'category'