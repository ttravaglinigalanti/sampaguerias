from django.urls import path

from . import views

app_name = 'hamburguer'
urlpatterns = [
    path('', views.HamburguerListView.as_view(), name='index'),
    path('search/', views.search_hamburguer, name='search'),
    path('create/', views.HamburguerCreateView.as_view(), name='create'),
    path('<int:pk>/', views.HamburguerDetailView.as_view(), name='detail'),
    path('update/<int:pk>/', views.HamburguerUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', views.HamburguerDeleteView.as_view(), name='delete'), 
    path('<int:hamburguer_id>/comment/', views.create_comment, name='create_comment'),
    path('lists/', views.CategoryListView.as_view(), name='lists'),
    path('lists/create/', views.CategoryCreateView.as_view(), name='create-list'),
    path('category/<int:pk>/', views.CategoryDetailView.as_view(), name='category_detail'),
]